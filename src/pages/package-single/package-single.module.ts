import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PackageSinglePage } from './package-single';

@NgModule({
  declarations: [
    PackageSinglePage,
  ],
  imports: [
    IonicPageModule.forChild(PackageSinglePage),
  ],
})
export class PackageSinglePageModule {}
