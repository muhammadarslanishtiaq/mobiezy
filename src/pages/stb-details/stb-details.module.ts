import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StbDetailsPage } from './stb-details';

@NgModule({
  declarations: [
    StbDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(StbDetailsPage),
  ],
})
export class StbDetailsPageModule {}
