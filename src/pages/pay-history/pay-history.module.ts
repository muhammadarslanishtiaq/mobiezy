import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PayHistoryPage } from './pay-history';

@NgModule({
  declarations: [
    PayHistoryPage,
  ],
  imports: [
    IonicPageModule.forChild(PayHistoryPage),
  ],
})
export class PayHistoryPageModule {}
